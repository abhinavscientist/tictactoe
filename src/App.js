import React from 'react';
import Square from './Square';

class App extends React.Component{
    constructor(props){
        super(props);
        this.state = {tictacarray : Array(9).fill('-')};
        this.player = 0;
        this.symbol = {0:'O', 1:'X'};
    }

    componentDidUpdate(){
        console.log('componentDidUpdate');
        this.checkWinner();
    }

    renderSquare = (i) => {
        return <Square onButtonClick={this.onButtonClick} index={i} value={this.state.tictacarray[i]}></Square>
    }

    onButtonClick = async (event) => {
        let index = event.target.value;
        
        let temp = Object.assign([], this.state.tictacarray);
        temp[index] = this.symbol[this.player];
        this.setState({tictacarray: temp});
        console.log('onButtonClick');
    }

    checkWinner = (event) => {
        
        //Toggle switch
        let array = this.state.tictacarray;
        window.arr = array;

        let row1 = (array[0] === array[1]) && (array[1] === array[2] && array[1] !=='-');
        let row2 = (array[3] === array[4]) && (array[4] === array[5] && array[4] !=='-');
        let row3 = (array[6] === array[7]) && (array[7] === array[8] && array[7] !=='-');

        let col1 = (array[0] === array[3]) && (array[3] === array[6] && array[3] !=='-');
        let col2 = (array[1] === array[4]) && (array[4] === array[7] && array[4] !=='-');
        let col3 = (array[2] === array[5]) && (array[5] === array[8] && array[5] !=='-');

        let diagonal = (array[0] === array[4]) && (array[4] === array[8] && array[4] !=='-');
        
        let isWin  = ( row1 || row2 || row3 || col1 || col2 || col3 || diagonal );
        // console.log(`iswin value is: ${isWin}`);
        if(isWin){
            alert(`Player: ${this.player} is the winner` );
        } else{
            this.player = (this.player === 0)? 1:0;
        }
        console.log('in winner');

    }

    render(){
        return (
            <div>
                <button disabled={this.player === 1}>Player 0</button>
                <button disabled={this.player === 0}>Player 1</button>
                <div>
                    {this.renderSquare(0)}
                    {this.renderSquare(1)}
                    {this.renderSquare(2)}
                </div>
                <div>
                    {this.renderSquare(3)}
                    {this.renderSquare(4)}
                    {this.renderSquare(5)}
                </div>
                <div>
                    {this.renderSquare(6)}
                    {this.renderSquare(7)}
                    {this.renderSquare(8)}
                </div>
                {/* <button onClick={this.checkWinner}>checkWinner</button> */}
            </div>
        );
    }
}

export default App;