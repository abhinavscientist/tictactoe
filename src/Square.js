import React from 'react';

class Square extends React.Component{

    render(){
        // let display_values = {0:'-', 1:'O', 2:'X'};
        // console.log(this.props.value);
        // console.log(display_values[this.props.value]);
        return (
            <button onClick={this.props.onButtonClick} value={this.props.index}>{this.props.value}</button>
        );
    }
}

export default Square;